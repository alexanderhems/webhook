try{
  var messagejson = require('./deploy-message.json');
  var packagejson = require('./package.json');
  var os = require('os');
  var request = require('request');
  var md5 = require('md5');
  var gitToObject = require('git-log-as-object');

  gitToObject.gitLog({includeKeys:['refs']})
  .then(commit => replace(commit[0]))
  .catch();


  function replace(commit){
    var push = JSON.stringify(messagejson);
    push = replaceFunction(push,'PROFILE',os.hostname());
    push = replaceFunction(push,'TITLE',packagejson.name);
    push = replaceFunction(push,'HASH',commit.partialHash);
    push = replaceFunction(push,'MESSAGE',commit.subject);
    push = replaceFunction(push,'TIMESTAMP',new Date().toString());
    push = replaceFunction(push,'COMMITTIMESTAMP',convertTimeStamp(commit.commitTime));
    push = replaceFunction(push,'AUTHOR',commit.author.name);
    push = replaceFunction(push,'EMAIL',commit.author.email);
    push = replaceFunction(push,'IMGURL',`https://www.gravatar.com/avatar/${md5(commit.author.email)}`);
    push = replaceFunction(push,'BRANCH',commit.refs[commit.refs.length-1]);
    console.log(push);
    pushToWebhook(push);
  }


  function pushToWebhook(message){
    request.post(packagejson.webhookUrl, {body: message})

  }


  function replaceFunction(content,prop,value){
    return content.replace(new RegExp(`{{${prop}}}`,'g'),value);
  }

  function convertTimeStamp(timestamp){
      var parsed = Date.parse(timestamp)
      return new Date(parsed).toString();
  }

} catch(error) {
  // Prevent Build Crash if notify breaks
}
